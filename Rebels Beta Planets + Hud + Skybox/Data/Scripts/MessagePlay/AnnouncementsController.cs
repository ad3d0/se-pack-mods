﻿/*
 *  Copyright (C) Chris Courson, 2016. All rights reserved.
 * 
 *  This file is part of MessagePlay, a Space Engineers mod available through Steam.
 *
 *  MessagePlay is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MessagePlay is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MessagePlay.  If not, see <http://www.gnu.org/licenses/>.
 */
using System;
using System.Collections.Generic;
using System.IO;
using System.Timers;
using Sandbox.ModAPI;

namespace MessagePlay
{
    public class Announcements
    {
		public bool enabled;
        public int intervalMinutes;
		public List<Announcement> announcements;
		public List<ScheduledAnnouncement> scheduledAnnouncements;
		int index;
		double intervalAccumulator;
		const double timerInterval = 15000;
		Timer intervalTimer;

        public Announcements()
        {
            announcements = new List<Announcement>();
			scheduledAnnouncements = new List<ScheduledAnnouncement>();
            index = 0;
			intervalAccumulator = 0;
        }

		public Announcement GetNextAnnouncement()
		{
			Announcement _announcement = announcements[index];
			index = (index + 1) % announcements.Count;
			return _announcement;
		}

		public void InitTimer() 
		{
			foreach (var item in scheduledAnnouncements)
			{
				item.ParseTime();
			}

			intervalTimer = new Timer()
			{
				AutoReset = true,
				Interval = timerInterval,
				Enabled = true
			};
			intervalTimer.Elapsed += Timer_Elapsed;
		}

		void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			intervalAccumulator += intervalTimer.Interval;

			DateTime now = DateTime.Now;

			// Reload scheduled announcements at midnight.
			if ((now - DateTime.Today).TotalMilliseconds < timerInterval)
			{
				Announcements _announcements = Configuration<Announcements>.LoadFromLocalStorage();
				scheduledAnnouncements = _announcements.scheduledAnnouncements;
			}

			foreach (ScheduledAnnouncement item in scheduledAnnouncements)
			{
				try
				{
					double milliseconds = (now - item.GetDateTime()).TotalMilliseconds;
					if (milliseconds > 0 && milliseconds < timerInterval)
					{
						new Message(MessageType.ScheduledAnnouncement, Configuration<ScheduledAnnouncement>.ToXML(item)).SendToOthers();
					}
				}
				catch (Exception ex)
				{
					MyAPIGateway.Utilities.ShowMessage("Exception", item.message + " - " +ex.Message);
				}
			}

			if (enabled && intervalAccumulator >= (intervalMinutes * 60000))
			{
				intervalAccumulator = 0;
				new Message(MessageType.Announcement, Configuration<Announcement>.ToXML(GetNextAnnouncement())).SendToOthers();
			}
		}

		public void CloseTimer()
		{
			if (intervalTimer != null) intervalTimer.Close();
		}
}

    public class Announcement
    {
        public string sender;
        public string message;

        public Announcement()
        {
			sender = null;
			message = null;
        }
	}

	public class ScheduledAnnouncement : Announcement
	{
		public string datetime;
		DateTime _datetime;

		public bool ParseTime()
		{
			return DateTime.TryParse(datetime, out _datetime);
		}

		public DateTime GetDateTime()
		{
			return _datetime;
		}
	}
}
