﻿using System;

namespace Cython.EnergyShields
{
	public class DamageMultiplierDefinition
	{
		public float DeformationDamageMultiplier = 2.5f;
		public float BulletDamageMultiplier = 0.3f;
		public float ExplosionDamageMultiplier = 0.9f;
		public float GrindDamageMultiplier = 0.5f;
		public float DrillDamageMultiplier = 0.5f;

		public DamageMultiplierDefinition ()
		{
		}
	}
}

