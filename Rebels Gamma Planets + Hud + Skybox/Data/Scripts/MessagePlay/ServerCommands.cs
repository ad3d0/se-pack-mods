#region pre_script
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using Sandbox.Common;
//using Sandbox.Common.Components;
//using Sandbox.Common.ObjectBuilders;
using Sandbox.Definitions;
using Sandbox.Engine;
using Sandbox.ModAPI;
using Sandbox.ModAPI.Ingame;
using Sandbox.ModAPI.Interfaces;
using Sandbox.Game;
using VRage.ObjectBuilders;
using VRage.Game;
//using VRage.Components;

namespace Commands
    {
        [Sandbox.Common.MySessionComponentDescriptor(Sandbox.Common.MyUpdateOrder.AfterSimulation)]
        public class GPSUtilityCommands : MySessionComponentBase
        {
        #endregion pre_script

        public bool init = false;

        public bool doRange = false;
        public double range = 0.0;

        public void Init()
        {
            if (init == false)
            {
                MyAPIGateway.Utilities.ShowMessage("Rebels-Games.com", "Servers ALPHA, BETA, GAMMA, DELTA");
            }
            init = true;

            MyAPIGateway.Utilities.MessageEntered += ProcessMessage;
        }

        protected override void UnloadData()
        {
            init = false;

            MyAPIGateway.Utilities.MessageEntered -= ProcessMessage;

            base.UnloadData();
        }

        public override void UpdateAfterSimulation()
        {
            if (!init)
            {
                if (MyAPIGateway.Session == null)
                    return;
                Init();
            }

            if (doRange)
            {
                long playerID = MyAPIGateway.Session.Player.PlayerID;
                List<VRage.Game.ModAPI.IMyGps> gps = MyAPIGateway.Session.GPS.GetGpsList(playerID);
                foreach (VRage.Game.ModAPI.IMyGps g in gps)
                {
                    VRageMath.Vector3D playerPos = MyAPIGateway.Session.Player.GetPosition();

                    double distance = VRageMath.Vector3D.Distance(playerPos, g.Coords);

                    if (distance > range)
                    {
                        MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), false);
                    }
                    else
                    {
                        MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), true);
                    }
                }
            }
        }

        public void WriteMessage(string message)
        {
            MyAPIGateway.Utilities.ShowMessage("Info", message);
            MyAPIGateway.Utilities.ShowNotification(message, 2000, MyFontEnum.Blue);
        }

        // gps help
        // gps hide|h <tag>
        // gps show|s <tag>
        // gps hideall|ha
        // gps showall|sa
        // gps showonly|so <tag>
        // gps cardinals
        // gps add|a [name] [sensor]
        // gps appendore|ao
        //
        // TDD:
        //  add ore data from sensors and appendore
        //  Add cardinals for planets
        //  filter beacons/antennas
        void ProcessMessage( string messageText, ref bool echo)
        {
            if (!messageText.StartsWith("/")) { return; }
            List <string> words = new List<string> (messageText.Trim().Replace("/", "").Split(' '));

            if(words.Count >= 1)
            {
                gpsProcess(words);
                log(messageText);   // testing
                echo = false;
            }
        }

        void gpsProcess(List<string> words )
        {
            switch (words[0])
            {
                case "donate":
                    string helpMessage2 =   " Credits in the game \n" +
                                            "\n" +
                                            "                   CREDITS \n" +
                                            "              1 € -   40.000 c.\n" +
                                            "             2 € -   80.000 c.\n" +
                                            "             5 € -  200.000 c.\n" +
                                            "            10 € -  400.000 c. +   400.000 c. for FREE! \n" +
                                            "            15 € -  600.000 c. +   900.000 c. for FREE! \n" +
                                            "            20 € -  800.000 c. +  2200.000 c. for FREE! \n" +
                                            "            30 € - 1200.000 c. +  3300.000 c. for FREE! \n" +
                                            "            45 € - 1600.000 c. +  4600.000 c. for FREE! \n" +
                                            "            60 € - 2400.000 c. + 7600.000 c. for FREE! \n" +
                                            "\n" +
                                            "              SUPER CREDITS \n" +
                                            "              1 € - 125 sc.\n" +
                                            "             2 € - 250 sc.\n" +
                                            "             5 € - 800 sc.\n" +
                                            "            10 € - 2000 sc. +       500 sc. for FREE! \n" +
                                            "            15 € - 3000 sc. +      1500 sc. for FREE! \n" +
                                            "            20 € - 4000 sc. +     3000 sc. for FREE! \n" +
                                            "            30 € - 6000 sc. +     4000 sc. for FREE! \n" +
                                            "            45 € - 9000 sc. +     9000 sc. for FREE! \n" +
                                            "            60 € - 12000 sc.+   38000 sc. for FREE! \n" +
                                            "\n" +
                                            "               The shop is at: www.rebels-games.com/shop\n" +
                                            "\n" +
                                            "               Enter you nick name and grid name in which you \n" +
                                            "               want the credits to appear\n" +
                                            "               Example: Nick - (Nickname of ship)\n" +
                                            "              (Realization time 24h)\n" +
                                            "\n" +
                                            "              Adds coordinates of station (Command: /gps)\n" +
                                            "\n" +
                                            "                (All funds allocated for server maintenance)";

                    MyAPIGateway.Utilities.ShowMissionScreen("Donate", "paypal.me/lostspace", " ", helpMessage2, null, "OK");

                    break;

                case "discord":
                    string helpMessage5 =   "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" + "\n" +
                                            "                                         Discord  Server Invite Link \n" +
                                            "\n" + "                                               discord.gg/KWmrpey \n";

                    MyAPIGateway.Utilities.ShowMissionScreen("Discord", "Voice Communicator", " ", helpMessage5, null, "OK");

                    break;

                case "joinhelp":
                    string helpMessage7 =
                        "                   To jump on another server and move your ship\n" +
                        "\n" +
                        " Use command chat:\n" +
                        " /join 1 - Alpha\n" +
                        " /join 2 - Beta\n" +
                        " /join 3 - Gamma\n" +
                        " /join 4 - Delta\n" +
                        " /join 5 - [Work in Progress]\n" +
                        "\n" +
                        " /joinstatus - Shows jump status.\n" +
                        "\n" +
                        "Max 1500 blocks.\n" +
                        "Be sure to have a medical room\n" +
                        "After the jump choose 'Space suit' to not lose the ship.\n" +
                        "\n" +
                        "If you select 'Space Suit' for the second time, will lose access to you objects.\n" +
                        "So be aware! \n" +
                        "\n" +
                        "                     (Remember! You're jumping on your own risk!)\n" +
                        "              All objects lost during server jump will not be restored!";

                    MyAPIGateway.Utilities.ShowMissionScreen("Join Server", "Rebels-Games.com", " ", helpMessage7, null, "OK");

                    break;

                case "help":
                    string helpMessage6 =   "\n" + 
                                            "                                             Administrator: TimPL \n" + 
                                            "\n" + 
                                            "Simple commands: \n" + 
                                            "!grids list    - Find your objects \n" + 
                                            "/help          - This text \n" + 
                                            "/joinhelp    - Jump on another server \n" + 
                                            "/sectors    - Seizing sectors \n" + 
                                            "/oreinfo     - Ore destiny \n" + 
                                            "/discord    - Discord invite link \n" +  
                                            "/donate     - How to donate for this project - Information about credits \n" + 
                                            "/motd        - Server rules \n" + 
                                            "/gps          - Adds all coordinates of station/monoliths \n" + 
                                            "\n" + 
                                            "\n" + 
                                            "                           Website \n" + 
                                            "                www.rebels-games.com \n" + 
                                            "\n" +
                                            "                           Donate                                                  Discord \n" + 
                                            "            www.rebels-games.com/donate              discord.gg/KWmrpey \n";

                    MyAPIGateway.Utilities.ShowMissionScreen("HELP", "commands", " ", helpMessage6, null, "OK");

                    break;

                case "gps":
                    long playerID = MyAPIGateway.Session.Player.PlayerID;
                    double maxD = 1000000000.0;
                    List<VRage.Game.ModAPI.IMyGps> gps = MyAPIGateway.Session.GPS.GetGpsList(playerID);

                    // Check the existing list, if there is a GPS coord at zplus then niavely assume they have already run this command ;)
                    foreach (VRage.Game.ModAPI.IMyGps g in gps) {
                        if( g.Coords.X == 0.0 && g.Coords.Y == 0.0 && g.Coords.Z == maxD )
                        {
                            WriteMessage("Rebels-Games.com");
                            return;
                        }
                    }
                    VRage.Game.ModAPI.IMyGps origin = MyAPIGateway.Session.GPS.Create("Hoth", "", new VRageMath.Vector3D(277511.0, -228658.0, 829104.0),true);
                    VRage.Game.ModAPI.IMyGps zplus  = MyAPIGateway.Session.GPS.Create("Menus",   "", new VRageMath.Vector3D(-532607.0, -105911.0, -878071.0), true);
                    VRage.Game.ModAPI.IMyGps zminus = MyAPIGateway.Session.GPS.Create("Europe",     "", new VRageMath.Vector3D(-1218808.0, 702272.0, -269066.0), true);
                    VRage.Game.ModAPI.IMyGps qplus  = MyAPIGateway.Session.GPS.Create("Neutral Station",  "", new VRageMath.Vector3D(16993.0, 24281.0, -5837.0), true);

                    MyAPIGateway.Session.GPS.AddGps(playerID, origin);
                    MyAPIGateway.Session.GPS.AddGps(playerID, zplus);
                    MyAPIGateway.Session.GPS.AddGps(playerID, zminus);
                    MyAPIGateway.Session.GPS.AddGps(playerID, qplus);

                    break;
                case "oreinfo":
                    string helpMessage =    "\n" +
                                            "              Asteroids Destiny Ore:                      Planets Destiny Ore: \n" +
                                            "              + Cobalt:            Yes                          + Cobalt:            Yes \n" +
                                            "              + Gold:               Yes                          + Gold:               Yes \n" +
                                            "              + Iron:                Yes                          + Iron:                Yes \n" +
                                            "              + Lateryt:           No                           + Lateryt:           Yes \n" +
                                            "              - Magnesium:    No                            - Magnesium:    Yes \n" +
                                            "              + Malachit:        Yes                          + Malachit:         Yes \n" +
                                            "              - Nickel:             No                           + Nickel:             Yes \n" +
                                            "              + Palladium:      Yes                          - Palladium:       No \n" +
                                            "              - Platinum:         No                           + Platinum:         Yes \n" +
                                            "              - Silicon:            No                           + Silicon:            Yes \n" +
                                            "              + Silver:             Yes                         + Silver:              Yes \n" +
                                            "              + Stone:             Yes                         + Stone:              Yes \n" +
                                            "              - Uranium:          No                           + Uranium:         Yes \n" +
                                            "\n" +
                                            "                                          Alpha Server - Dead Sun Ore \n" +
                                            "                                         Unknown Material    ->     Cez \n" +
                                            "                                         Unknown Material    ->     Rad \n" +
                                            "                                         Unknown Material    ->     Kaliforn \n" +
                                            "                                         Unknown Material    ->     Diamond \n" +
                                            "\n" +
                                            "                                                           Refining \n" +
                                            "                                         Custom Ore        to      Ingot \n" +
                                            "                                         Lateryt                ->     Aluminium \n" +
                                            "                                         Malachit             ->     Copper \n" +
                                            "                                         Palladium Ore    ->     Palladium \n" +
                                            "\n" +
                                            "                                                      IN Refinery: \n" +
                                            "                                                      - All Ore \n" +
                                            "                                                      - Scrap Metal \n" +
                                            "\n" +
                                            "                                                  IN Arc Furnace: \n" +
                                            "                                                  - Cobalt Ore \n" +
                                            "                                                  - Iron Ore \n" +
                                            "                                                  - Nickel Ore \n" +
                                            "                                                  - Scrap Metal \n" + "\n";

                    MyAPIGateway.Utilities.ShowMissionScreen("Ore Density", "Rebels-Games.com", " ", helpMessage, null, "OK");

                    break;
                case "range":
                case "r:":
                    if( words.Count == 2 )
                    {
                        if (words[1] == "off")
                        {
                            doRange = false;
                            filterGPS(false, true, false, false, "");
                        }
                        else {
                            decimal r;
                            if( decimal.TryParse(words[1],out r) )
                            {
                                doRange = true;
                                range = (double)r;
                            } else
                            {
                                MyAPIGateway.Utilities.ShowMessage("GPS", "Unknown range argument: " + words[1]);
                            }
                        }
                    }
                    break;
                case "sectors":
                    string helpMessage3 =   "Works are in progress\n" +
                                            "\n" +
                                            "Fraction can take any sector in the world game.\n" +
                                            "Determinant of each sector is a planet.\n" +
                                            "To occupy the sector has been approved\n" +
                                            "Just inform the administrator\n" +
                                            "And take over Monolith\n" +
                                            "Sector - 250km from the planet.\n" +
                                            "LCD Block was equips the map sectors.\n" +
                                            "\n" +
                                            "Monolith (/gps - Adds all coordinates)\n" +
                                            "\n" +
                                            "                              Warning\n" +
                                            "The acquisition of a monolith without representative faction\n" +
                                            "                  will be severely punished";

                    MyAPIGateway.Utilities.ShowMissionScreen("Sectors", "Rebels-Games.com", " ", helpMessage3, null, "OK");

                    break;
                

            }
        }

        // Filter out the appropriate GPS coords.
        void filterGPS( bool hideAll, bool showAll, bool hide, bool show, string tag)
        {
            long playerID = MyAPIGateway.Session.Player.PlayerID;
            List<VRage.Game.ModAPI.IMyGps> gps = MyAPIGateway.Session.GPS.GetGpsList(playerID);
            foreach (VRage.Game.ModAPI.IMyGps g in gps)
            {
                if (hideAll == true || (hide == true && (g.Name.Contains(tag) || g.Description.Contains(tag))))
                {
                    MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), false);
                }
                if (showAll == true || (show == true && (g.Name.Contains(tag) || g.Description.Contains(tag))))
                {
                    MyAPIGateway.Session.GPS.SetShowOnHud(playerID, g.GetHashCode(), true);
                }
            }
        }

        // Writes to SpaceEngineers/Storage
        void log( string text )
        {
            using (TextWriter writer = MyAPIGateway.Utilities.WriteFileInGlobalStorage("Log.txt"))
            {
                writer.WriteLine(text);
            }
        }

        #region post_script
    }
    }
#endregion post_script