/*
 *  Copyright (C) Chris Courson, 2016. All rights reserved.
 * 
 *  This file is part of MessagePlay, a Space Engineers mod available through Steam.
 *
 *  MessagePlay is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  MessagePlay is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with MessagePlay.  If not, see <http://www.gnu.org/licenses/>.
 */
using Sandbox.ModAPI;
using System.Text;

namespace MessagePlay
{
    public class Message
    {
        public ulong playerId;
        public MessageType messageType;
        public string message;

        public Message()
        {

        }

        public Message(MessageType messageType, string message)
        {
            playerId = MyAPIGateway.Multiplayer.MyId;
            this.messageType = messageType;
            this.message = message;
        }

        public Message(byte[] bytes)
        {
            string xml = Encoding.Unicode.GetString( bytes);
            Message _message = MyAPIGateway.Utilities.SerializeFromXML<Message>(xml);
            this.playerId = _message.playerId;
            this.messageType = _message.messageType;
            this.message = _message.message;
        }

        public byte[] GetBytes()
        {
            string xml = MyAPIGateway.Utilities.SerializeToXML(this);
            return Encoding.Unicode.GetBytes(xml);
        }

        public void SendToServer()
        {
            MyAPIGateway.Multiplayer.SendMessageToServer(1001, GetBytes());
        }

        public void SendTo(ulong id)
        {
            MyAPIGateway.Multiplayer.SendMessageTo(1001, GetBytes(), id);
        }

        public void SendToOthers()
        {
            MyAPIGateway.Multiplayer.SendMessageToOthers(1001, GetBytes());
        }

        public static void RequestWelcome()
        {
            MyAPIGateway.Multiplayer.SendMessageToServer(1001, new Message(MessageType.WelcomeRequest, null).GetBytes());
        }

        public static void RequestHelp()
        {
            MyAPIGateway.Multiplayer.SendMessageToServer(1001, new Message(MessageType.HelpRequest, null).GetBytes());
        }

        public static void RequestReload()
        {
            MyAPIGateway.Multiplayer.SendMessageToServer(1001, new Message(MessageType.ReloadRequest, null).GetBytes());
        }
    }

    public enum MessageType
    {
        WelcomeRequest,
        WelcomeResponse,
        HelpRequest,
        HelpResponse,
        Announcement,
		ScheduledAnnouncement,
        ReloadRequest,
        ReloadResponse
    }
}